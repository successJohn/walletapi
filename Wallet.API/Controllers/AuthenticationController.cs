﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Wallet.Entities.DTO;
using Wallet.Entities.Models;
using Wallet.Services.Interfaces;

namespace Wallet.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AuthenticationController : ControllerBase
    {
        private readonly UserManager<User> _userManager;
        private readonly IConfiguration _configuration;
        private readonly IAuthenticationManager _authManager;

        public AuthenticationController(UserManager<User> userManager, IConfiguration configuration, IAuthenticationManager authManager)
        {
            _userManager = userManager;
            _configuration = configuration;
            _authManager = authManager;
        }

        [HttpPost]
        [Route("Register")]


        public async Task<IActionResult> Register([FromBody] RegisterModel model)
        {

            var existingUser = await _userManager.FindByNameAsync(model.UserName);
           


            User newUser = new User()
            {
                FirstName = model.FirstName,
                LastName = model.LastName,
                Email = model.Email,
                UserName = model.UserName,
            };

            var result = await _userManager.CreateAsync(newUser, model.Password);
            if (!result.Succeeded)
            {

               
                return BadRequest();
            }

            return StatusCode(201);
        }


        [HttpPost("login")]

        public async Task<IActionResult> Authenticate([FromBody] LoginModel loginModel)
        {
            if (!await _authManager.ValidateUser(loginModel))
            {
                
                return Unauthorized();
            }
            return Ok(new { Token = await _authManager.CreateToken() });
        }
    }
}
