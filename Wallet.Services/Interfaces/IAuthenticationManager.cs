﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Wallet.Entities.DTO;

namespace Wallet.Services.Interfaces
{
    public interface IAuthenticationManager
    {
        Task<bool> ValidateUser(LoginModel loginModel);
        Task<string> CreateToken();
    }
}
